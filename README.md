# 关于
这是关于 ELF 文件格式的一些笔记，带有强烈的个人风格，不可作为任何参考或引用。如有需要请参考 [原文](https://paper.seebug.org/papers/Archive/refs/elf/Understanding_ELF.pdf)

# ELF 格式解析
 -- 基于ELF规范v1.2版本

关于 ELF, 即 Executable and Linking Format, 即可执行可链接格式，最开始是 UNIX 系统实验室作为应用程序二进制接口(Application Binary Interface - ABI) 的一部分而定制和发布。作为一种可移植的格式，被 TIS 应用到 Intel 架构 32 位计算机的各种操作系统上。

最大特定是 广泛的 `适用性`，二进制接口定义很通用，可以让它平滑的移植到不同的操作环境上。

ELF 文件格式规范由 TIS (Tool Interface Standards) 委员会制定 - 一个微型计算机工业的联合组织。TIS 致力于为32位操作系统下的开发工具提供标准化的软件接口：包括目标标志格式、可执行文件格式、调试信息格式。

TIS 目标是在不同硬件上建立起统一的软件规范，使软件在不同硬件上有尽可能大的移植性。

与TIS的其它各种规范一样，ELF规范是基于已存在的、并已被事实证明可行的格式而制定的，而且这种格式已经被TIS成员们广泛地使用在他们的软件系统中。`所以当格式不能满足要求而需要改进的时候，应首先考虑扩展已有的规范而不是制定新的`。

考虑到不同硬件的和操作系统的适用性扩展性， ELF(v1.2)规范在制定时，把ELF格式分为了三个层次。
1. 基本部分
2. 处理器扩展部分，会因为处理器架构的不同而不同，规范中只有 Intel i386的内容，其他处理器由厂商提供
3. 操作系统的扩展部分从，规范中只包含 UNIX System V Release 4

## 名词对照表
中文名词|原文
:--|:--
可执行可连接格式|ELF
ELF文件头|ELF        header
基地址|base        address
动态连接器|dynamic linker
动态连接|dynamic        linking
全局偏移量表|global offset table
哈希表|hash        table
初始化函数|initialization        function
连接编辑器|link        editor
目标文件|object        file
函数连接表|procedure linkage table
程序头|program        header
程序头表|program header table
程序解析器|program        interpreter
重定位|relocation
共享目标|shared        object
节|section
节头|section        header
节头表|section header table
段|segment
字符串表|string        table
符号表|symbol        table
终止函数|termination        function  


## 章节索引
1. [ELF 文件的静态结构](./01.%E9%9D%99%E6%80%81%E7%BB%93%E6%9E%84.md)
2. [ELF 文件的装载与动态链接](./2.ELF%E6%96%87%E4%BB%B6%E7%9A%84%E8%A3%85%E8%BD%BD%E4%B8%8E%E5%8A%A8%E6%80%81%E8%BF%9E%E6%8E%A5.md)
3. [自己的话](./04.%E5%BF%83%E5%BE%97.md)