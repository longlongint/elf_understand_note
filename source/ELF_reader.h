#ifndef ELF_PARSER_H
#define ELF_PARSER_H

#include <cstdint>
#include <string>
#include <elf.h>

namespace ELF
{

class ELF_reader
{
public:
    ELF_reader() = delete;
    explicit ELF_reader(const std::string& file_path);
    ELF_reader(const ELF_reader& object) = delete;
    ELF_reader(ELF_reader&& object) noexcept;
    ELF_reader& operator=(const ELF_reader& object) = delete;
    ELF_reader& operator=(ELF_reader&& object) noexcept;
    ~ELF_reader();

    int show_file_header() const;
    int show_section_headers() const;
    std::string get_relocation_type(Elf64_Xword &);
    int print_rel_symbol_value(Elf64_Xword &sym_idx,Elf64_Sxword &r_addend, const Elf64_Shdr *section_table);
    int show_symbols() const;
    int show_rel();

private:
    void load_memory_map();
    void close_memory_map();
    void initialize_members(std::string file_path = std::string(),
                            int fd = -1,
                            std::size_t program_length = 0,
                            std::uint8_t *mmap_program = nullptr);

    std::string file_path_;
    int fd_;
    std::size_t program_length_;
    std::uint8_t *mmap_program_;
};

} // namespace ELF

#endif // ELF_PARSER_H