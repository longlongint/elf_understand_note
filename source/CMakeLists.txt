# CMake 最低版本号要求
cmake_minimum_required (VERSION 2.8...3.21)

set(CMAKE_C_STANDARD 99)
set(CMAKE_CXX_STANDARD 11)
set(CMAKE_POSITION_INDEPENDENT_CODE ON)
set(CMAKE_CXX_FLAGS "-O0 -g -Werror ")
set(CMAKE_C_FLAGS "-O0 -g -Werror ")
set(CMAKE_EXE_LINKER_FLAGS "-Wl,-q ")
set(CMAKE_BUILD_TYPE "Debug")

# 设置一些输出的变量名
set(This ELF_UNDERSTAND)
set(ExecutableName main)
set(LibraryName readElf)

# 项目信息
project (${This} C CXX)

# 将需要测试的函数放到一个库里面
set(Headers ELF_reader.h)
set(Sources ELF_reader.cpp)
add_library(${LibraryName} STATIC ${Sources} ${Headers})
include_directories(.)

# 设置主程序
add_executable(${This} main.cpp)
SET_TARGET_PROPERTIES(${This} PROPERTIES OUTPUT_NAME ${ExecutableName})
target_link_libraries(${This} PUBLIC ${LibraryName})

# googletest
set(TestExecutableName ELF_TEST)
add_subdirectory (googletest)
add_subdirectory (test)

# 启用测试，并使用 googletest 进行测试
enable_testing()
add_test (googletestAll test/${TestExecutableName})
add_test (basicRunTest ${ExecutableName})



