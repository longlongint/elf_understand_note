#include "ELF_reader.h"

int main(int argc, char *argv[])
{
    using ELF::ELF_reader;

    char *fileName = argc > 1?argv[1]:argv[0];
    ELF_reader s(fileName);

    //s.show_file_header(); // readelf -h
    //s.show_section_headers(); //readelf -S
    //s.show_symbols();   // readelf -s
    s.show_rel();// readelf -r
    return 0;
}